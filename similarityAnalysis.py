__author__ = 'jugpreet'

import nltk
import string
import os
import sys

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from prepareCodeDirectory import buildEventDictonary
from heapq import heappush, heapreplace, heappop

path = 'newsFiles/'
token_dict = {}
stemmer = PorterStemmer()

def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def tokenize(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems



def main(argv):
    eventsDictionary = buildEventDictonary(argv[0])

    megaDictionary = {}
    #get all files in a dictionary
    for subdir, dirs, files in os.walk(path):
        for file in files:
             if file.endswith(".txt"):
                file_path = subdir + os.path.sep + file
                shakes = open(file_path, 'r')
                text = shakes.read()
                lowers = text.lower()
                #no_punctuation = lowers.translate(None, string.punctuation)
                #token_dict[file] = no_punctuation
                token_dict[file_path] = lowers


    #prepare the tokenizer
    tfidf = TfidfVectorizer(tokenizer=tokenize, stop_words='english')
    #print(tfidf)
    for key in eventsDictionary.keys():
        eventCode = key
        eventName = eventsDictionary[key]["eventName"]
        importantWords = eventsDictionary[key]["importantWords"]
        uniqueWords = set(importantWords)
        importantWordsString = string.join(uniqueWords, " ")

        documentDetails = {}
        heap = []
        maxDocuments =300
        for doc in token_dict.keys():

            val = str(token_dict[doc]).strip()
            val = val.replace("\n","")
            translatedVal = val.translate(None, string.punctuation)
            translatedVal = translatedVal.split()
            length = 400
            if len(translatedVal) < 400:
                length = len(translatedVal)
            translatedVal = translatedVal[0:length]
            translatedVal = string.join(translatedVal, " ")

            tfs = tfidf.fit_transform([importantWordsString, translatedVal])
            similarityMatrix = (tfs*tfs.T).A #cosine similarity
            similarityValue = similarityMatrix[0,1]

            exampleAndScore = [similarityValue, translatedVal, doc]
            #consider similarity value greater than 0.025
            if similarityValue > 0.04 :
                #consider only top k documents to be put under a category based on the similarity value
                if maxDocuments >=0:
                    heappush(heap, exampleAndScore)
                    maxDocuments = maxDocuments-1
                else :
                    minSimilarity = heap[0][0]
                    if similarityValue > minSimilarity:
                        heapreplace(heap, exampleAndScore)

        #prepare a dictionary of the candidate documents.
        while heap:
            value = heappop(heap)
            details = [value[0], value[1]]
            documentDetails[value[2]] = details

        #write the example and score corresponding to the event code
        megaDictionary[eventCode] = documentDetails

    #write training data to file
    writeFile = open("trainingFile7FullDayThree", "w")
    for key in megaDictionary.keys():
        dic = megaDictionary[key]
        print(key + " "+ str(len(dic)))
        for f in dic.keys():
            writeFile.write(key)
            writeFile.write("\t")
            writeFile.write(str(dic[f][1]))
            writeFile.write("\n")


if __name__=="__main__" : main(sys.argv[1:])