# Script to covert text file to csv for monkey learn training data
import csv

txt_file = r"/Users/Shamvi/Documents/text-classification/040-training"
csv_file = r"/Users/Shamvi/Documents/Usc/NLP/upload/training040.csv"

in_txt = csv.reader(open(txt_file, "rb"), delimiter = '\t')
out_csv = csv.writer(open(csv_file, 'wb'))

out_csv.writerows(in_txt)