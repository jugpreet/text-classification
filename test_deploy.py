# Script to classify test data and store results
from monkeylearn import MonkeyLearn

# list of test files
filelist =["/Users/Shamvi/Documents/text-classification/TestData"]
text_list =[]
for file in filelist:
    fi = open(file,'r')
    input = fi.read()
    lines = input.splitlines()
    i=0
    for each in lines:
        i+=1
        part = each.split('\t')
        text_list.append(part[1])
    print(len(text_list))



ml = MonkeyLearn('7839eca966b27cf7a0bba27a17ee0ee9508758e8')
module_id = 'cl_DBdKe57Y'
res = ml.classifiers.classify(module_id, text_list, sandbox=True)
print res.result

# results stored in file
fo1 = open('nb-resultTest3-173-176-060-172-171-040','w')
for each in res.result:
    fo1.write(each[0]['label']+'\n')
print(len(res.result))