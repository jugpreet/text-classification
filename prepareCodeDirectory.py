__author__ = 'jugpreet'

import re

def buildEventDictonary(argv):

    eventsFileLoc = argv
    eventsFile = open(eventsFileLoc, "r")
    dictionary={}

    for line in eventsFile:
        line = str(line).strip()
        items = re.split(r'\t+', line)
        eventCode = items[0]
        #print(eventCode)
        eventName = items[1]
        eventName = eventName.lower()
        example = items[2]
        example = example.lower()
        importantWords = re.findall("\|(.*?)\|", example)
        example = example.replace('|', '')

        if eventCode in dictionary:
            examples = dictionary[eventCode]["examples"]
            examples.append(example)
            dictionary[eventCode]["examples"] = examples

            impWords = dictionary[eventCode]["importantWords"]
            impWords.extend(importantWords)
            dictionary[eventCode]["importantWords"] = impWords
        else :
            examples = []
            examples.append(example)
            tempDict={"eventName":eventName, "examples": examples, "importantWords" : importantWords}
            dictionary[eventCode]=tempDict

    print(dictionary)

    return dictionary
