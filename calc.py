#script to calculate accuracy

import sys
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.metrics import accuracy_score

#test output file
fi1 = open('nb-resultTest3-173-176-060-172-171-040',"r")
test = fi1.read()
test_lines = test.splitlines()
total = len(test_lines)


# lebelled file
dict_label ={}
list_label=[]
# list of test files in same order as in test deploy
filelist =["/Users/Shamvi/Documents/text-classification/TestData"]
for file in filelist:
    fi2 = open(file,"r")
    labelfile = fi2.read()
    label_lines =labelfile.splitlines()



    for each in label_lines:
        part = each.split('\t')
        list_label.append(int(part[0]))
        if part[0] not in dict_label.keys():
            dict_label[int(part[0])] = 1
    print(len(label_lines))

test_label=[]
for each in test_lines:
    part = each.strip()
    test_label.append(int(part))
print "test size"
print(len(test_lines))

labels = list(dict_label.keys())

testcat = {}
for each in list_label:
    if each not in testcat:
        testcat[each] = 1
    else:
        testcat[each]+=1

print testcat

print labels
print "confusion matirx"
print(confusion_matrix(list_label, test_label, labels))

print "accuracy"
print(accuracy_score(list_label, test_label))
print dict_label.keys()

